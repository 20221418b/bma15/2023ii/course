/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.lbarbaranf.operators;

/**
 *
 * @author Luis Barbaran <luis.barbaran.f@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.print("-");
        System.out.println("Operators!");
        System.out.print("+");
        
        int y1 = 8;
        double x1 = 3 + 2 * --y1;
        System.out.println("x1: "+x1);
        
        //aritmetic operators
        int x2 = 2 * 5 + 3 * 4 - 8;
        System.out.println("x2 = "+x2);
        
        int x3 = 2 * ((5 + 3) * 4 - 8);
        System.out.println("x3 = "+x3);
        
        System.out.println(9 / 3);
        System.out.println(9 % 3);
        System.out.println(10 / 3);
        System.out.println(10 % 3);
        System.out.println(11 / 3);
        System.out.println(11 % 3);
        System.out.println(12 / 3);
        System.out.println(12 % 3);
        
        int x4 = 1;
        long y4 = 33;
        System.out.println(x4 * y4);
        
        System.out.println("bytes: "+Integer.SIZE/8);
        System.out.println("bytes: "+Long.SIZE/8);
        
    }
}
